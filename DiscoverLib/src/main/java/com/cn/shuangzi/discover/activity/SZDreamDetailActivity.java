package com.cn.shuangzi.discover.activity;

import android.widget.TextView;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.DreamInfo;
import com.cn.shuangzi.discover.common.DiscoverConst;
import com.cn.shuangzi.discover.common.HttpRequest;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CN.
 */

public abstract class SZDreamDetailActivity extends SZBaseActivity implements SZInterfaceActivity{
    private String id;
//    private TextView txtDesc;
    private TextView txtDetail;


    @Override
    protected int onGetChildView() {
        return R.layout.activity_dream_detail;
    }

    @Override
    protected void onBindChildViews() {
//        txtDesc = findViewById(R.id.txtDesc);
        txtDetail = findViewById(R.id.txtDetail);
    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        showBackImgLeft(getBackImgLeft());
        id = getStringExtra();
        if(id == null){
            finish();
            return;
        }
        onReloadData(false);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        showBar();
        Map<String,String> params = new HashMap<>();
        params.put("id",id);
        HttpRequest.request(DiscoverConst.URL_DREAM_SEARCH_DETAIL, params, getRequestTag(), new HttpRequest.HttpResponseSimpleListener() {
            @Override
            public void onNetError(String url, int errorCode) {
                isShowError(true);
            }

            @Override
            public void onSuccess(String url, String responseData) {
                isShowContent(true);
                DreamInfo dreamInfo = new Gson().fromJson(responseData,DreamInfo.class);
                setTitleTxt(dreamInfo.getTitle());
//                txtDesc.setText(dreamInfo.getDes());
                StringBuffer content = new StringBuffer();
                for (String value : dreamInfo.getList()){
                    content.append(DiscoverConst.SPACE+value).append("\n");
                }
                txtDetail.setText(content.toString());
            }

        });
    }
}
