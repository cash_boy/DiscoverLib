package com.cn.shuangzi.discover.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.adp.DreamSearchResultListAdp;
import com.cn.shuangzi.discover.bean.ConstellationYear;
import com.cn.shuangzi.discover.bean.DreamInfo;
import com.cn.shuangzi.discover.common.DiscoverConst;
import com.cn.shuangzi.discover.common.HttpRequest;
import com.cn.shuangzi.util.SZIRecyclerViewUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CN.
 */

public abstract class SZDreamSearchResultListActivity extends SZBaseActivity implements SZInterfaceActivity {
    private RecyclerView recyclerView;
    private String searchContent;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_dream_search_result_list;
    }

    @Override
    protected void onBindChildViews() {
        recyclerView = findViewById(R.id.recyclerView);
    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        showBackImgLeft(getBackImgLeft());
        searchContent = getStringExtra();
        if(searchContent ==null){
            finish();
            return;
        }
        setTitleTxt(searchContent);
        onReloadData(false);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        showBar();
        Map<String, String> params = new HashMap<>();
        params.put("q", searchContent);
        params.put("full", "0");
        HttpRequest.request(DiscoverConst.URL_DREAM_SEARCH, params, getRequestTag(), new HttpRequest
                .HttpResponseSimpleListener() {
            @Override
            public void onNetError(String url, int errorCode) {
                isShowError(true);
            }

            @Override
            public void onSuccess(String url, String responseData) {
                isShowContent(true);
                final List<DreamInfo> dreamInfoList = new Gson().fromJson(responseData,new TypeToken<List<DreamInfo>>(){}.getType());
                DreamSearchResultListAdp dreamSearchResultListAdp = new DreamSearchResultListAdp(getActivity(),dreamInfoList);
                dreamSearchResultListAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        startActivity(dreamInfoList.get(position).getId(),getDreamDetailActivity());
                    }
                });
                SZIRecyclerViewUtil.setVerticalLinearLayoutManager(getActivity(),recyclerView);
                recyclerView.setAdapter(dreamSearchResultListAdp);
            }
        });
    }
    public abstract Class getDreamDetailActivity();
}
