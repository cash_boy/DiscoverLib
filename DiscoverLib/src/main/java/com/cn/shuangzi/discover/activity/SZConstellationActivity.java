package com.cn.shuangzi.discover.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.adp.ConstellationAdp;
import com.cn.shuangzi.discover.bean.ConstellationInfo;
import com.cn.shuangzi.util.SZIRecyclerViewUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CN.
 */

public abstract class SZConstellationActivity extends SZBaseActivity implements SZInterfaceActivity {
    private RecyclerView recyclerView;
    private List<ConstellationInfo> constellationInfoList;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_constellation;
    }

    @Override
    protected void onBindChildViews() {
        recyclerView = findViewById(R.id.recyclerView);
    }

    @Override
    protected void onBindChildListeners() {
    }

    @Override
    protected void onChildViewCreated() {
        setTitleTxt(R.string.txt_constellation);
        showBackImgLeft(getBackImgLeft());
        constellationInfoList = new ArrayList<>();
        constellationInfoList.add(new ConstellationInfo("白羊座", "3月21日 - 4月19日", R.mipmap.ic_baiyang));
        constellationInfoList.add(new ConstellationInfo("金牛座", "4月20日 - 5月20日", R.mipmap.ic_jinniu));
        constellationInfoList.add(new ConstellationInfo("双子座", "5月21日 - 6月21日", R.mipmap.ic_shuangzi));
        constellationInfoList.add(new ConstellationInfo("巨蟹座", "6月22日 - 7月22日", R.mipmap.ic_juxie));
        constellationInfoList.add(new ConstellationInfo("狮子座", "7月23日 - 8月22日", R.mipmap.ic_shizi));
        constellationInfoList.add(new ConstellationInfo("处女座", "8月23日 - 9月22日", R.mipmap.ic_chunv));
        constellationInfoList.add(new ConstellationInfo("天秤座", "9月23日 - 10月23日", R.mipmap.ic_tiancheng));
        constellationInfoList.add(new ConstellationInfo("天蝎座", "10月24日 - 11月22日", R.mipmap.ic_tianxie));
        constellationInfoList.add(new ConstellationInfo("射手座", "11月23日 - 12月21日", R.mipmap.ic_sheshou));
        constellationInfoList.add(new ConstellationInfo("摩羯座", "12月22日 - 1月19日", R.mipmap.ic_mojie));
        constellationInfoList.add(new ConstellationInfo("水瓶座", "1月20日 - 2月18日", R.mipmap.ic_shuiping));
        constellationInfoList.add(new ConstellationInfo("双鱼座", "2月19日 - 3月20日", R.mipmap.ic_shuangyv));
        SZIRecyclerViewUtil.setVerticalLinearLayoutManager(this, recyclerView);
        ConstellationAdp constellationAdp = new ConstellationAdp(constellationInfoList);
        constellationAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                startActivity(constellationInfoList.get(position).getName(), getDetailClass());
            }
        });
        recyclerView.setAdapter(constellationAdp);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
    }

    public abstract Class getDetailClass();
}
