package com.cn.shuangzi.discover.bean;

/**
 * Created by CN on 2017-12-21.
 */

public class ConstellationDay {
    private String date;
    private String name;
    private String datetime;
    private String all;
    private String color;
    private String health;
    private String love;
    private String money;
    private String number;
    private String QFriend;
    private String summary;
    private String work;
//    date : 20171221
//     : "摩羯座"
//     : "2017年12月21日"
//     : "40%"
//     : "黄色"
//     : "80%"
//     : "40%"
//     : "40%"
//     : 8
//     : "天蝎座"
//     : "今天财运上不是很顺畅，投资的人，会小有波动不利。"
//     : "40%"
//    resultcode : "200"
//    error_code : 0

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getDatetime() {
        return datetime;
    }

    public float getAll() {
        try {
            return Float.parseFloat(all)/10;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getColor() {
        return color;
    }

    public float getHealth() {
        try {
            return Float.parseFloat(health)/10;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public float getLove() {
        try {
            return Float.parseFloat(love)/10;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public float getMoney() {
        try {
            return Float.parseFloat(money)/10;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public String getNumber() {
        return number;
    }

    public String getQFriend() {
        return QFriend;
    }

    public String getSummary() {
        return summary;
    }

    public float getWork() {
        try {
            return Float.parseFloat(work)/10;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
