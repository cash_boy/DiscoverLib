package com.cn.shuangzi.discover.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cn.shuangzi.SZBaseFragment;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.ConstellationYear;
import com.cn.shuangzi.discover.common.DiscoverConst;
import com.cn.shuangzi.discover.common.HttpRequest;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by CN on 2017-12-21.
 */

public class ConstellationYearFragment extends SZBaseFragment {
    private String constellationName;
    private String type;
    private TextView txtDate;
    private LinearLayout lltStone;
    private TextView txtStone;
    private LinearLayout lltMiMa;
    private TextView txtMiMa;
    private LinearLayout lltHealth;
    private TextView txtHealth;
    private LinearLayout lltLove;
    private TextView txtLove;
    private LinearLayout lltWork;
    private TextView txtWork;
    private LinearLayout lltMoney;
    private TextView txtMoney;
    private String titleShow;
    public static ConstellationYearFragment newInstance(String type, String constellationName) {
        ConstellationYearFragment fragment = new ConstellationYearFragment();
        Bundle args = new Bundle();
        args.putString("name", constellationName);
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int onGetChildView() {
        return R.layout.fragment_constellation_year;
    }

    @Override
    protected void onBindChildViews() {
        txtDate = findViewById(R.id.txtDate);
        lltStone = findViewById(R.id.lltStone);
        txtStone = findViewById(R.id.txtStone);
        lltMiMa = findViewById(R.id.lltMiMa);
        txtMiMa = findViewById(R.id.txtMiMa);
        txtHealth = findViewById(R.id.txtHealth);
        lltHealth = findViewById(R.id.lltHealth);
        txtLove = findViewById(R.id.txtLove);
        lltLove = findViewById(R.id.lltLove);
        txtWork = findViewById(R.id.txtWork);
        lltWork = findViewById(R.id.lltWork);
        txtMoney = findViewById(R.id.txtMoney);
        lltMoney = findViewById(R.id.lltMoney);
    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        initParam();
    }

    @Override
    public void onVisible() {
        super.onVisible();
        if(isFirstLoad()){
            initParam();
            setFirstLoad(false);
            onReloadData(false);
        }
    }
    private void initParam(){
        if(constellationName == null) {
            constellationName = getArguments().getString("name");
            type = getArguments().getString("type");
        }
        if(constellationName == null){
            getActivity().finish();
            return;
        }
    }
    @Override
    protected void onReloadData(boolean isRefresh) {
        showBar();
        Map<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("consName", constellationName);
        HttpRequest.request(DiscoverConst.URL_CONSTELLATION, params, getRequestTag(), new HttpRequest
                .HttpResponseSimpleListener() {
            @Override
            public void onNetError(String url, int errorCode) {
                isShowError(true);
            }

            @Override
            public void onSuccess(String url, String responseData) {
                isShowContent(true);
                ConstellationYear constellationYear = new Gson().fromJson(responseData,ConstellationYear.class);
                initView(constellationYear);
            }
        });
    }
    private void initView(ConstellationYear constellationYear){
//        if(SZValidatorUtil.isValidString(constellationYear.getDate())){
//            txtDate.setVisibility(View.VISIBLE);
//            txtDate.setText(constellationYear.getDate());
//        }
        if(SZValidatorUtil.isValidString(constellationYear.getLuckeyStone())){
            lltStone.setVisibility(View.VISIBLE);
            txtStone.setText(DiscoverConst.SPACE+constellationYear.getLuckeyStone());
        }
        if(SZValidatorUtil.isValidList(constellationYear.getMima().getText())){
            lltMiMa.setVisibility(View.VISIBLE);
            StringBuffer content = new StringBuffer();
            if(SZValidatorUtil.isValidString(constellationYear.getMima().getInfo())){
                content.append(DiscoverConst.SPACE+"短评："+constellationYear.getMima().getInfo()+"\n");
            }
            for (int i=0;i< constellationYear.getMima().getText().size();i++ ) {
                String text = constellationYear.getMima().getText().get(i);
                if(SZValidatorUtil.isValidString(text)) {
                    content.append(DiscoverConst.SPACE+text);
                    if ((i + 1) < constellationYear.getMima().getText().size()) {
                        content.append("\n");
                    }
                }
            }
            txtMiMa.setText(content.toString());
        }
        if(SZValidatorUtil.isValidList(constellationYear.getHealth())){
            lltHealth.setVisibility(View.VISIBLE);
            StringBuffer content = new StringBuffer();
            for (int i=0;i< constellationYear.getHealth().size();i++ ) {
                String text = constellationYear.getHealth().get(i);
                if(SZValidatorUtil.isValidString(text)) {
                    content.append(DiscoverConst.SPACE+text);
                    if ((i + 1) < constellationYear.getHealth().size()) {
                        content.append("\n");
                    }
                }
            }
            txtHealth.setText(content.toString());
        }
        if(SZValidatorUtil.isValidList(constellationYear.getCareer())){
            lltWork.setVisibility(View.VISIBLE);
            StringBuffer content = new StringBuffer();
            for (int i=0;i< constellationYear.getCareer().size();i++ ) {
                String text = constellationYear.getCareer().get(i);
                if(SZValidatorUtil.isValidString(text)) {
                    content.append(DiscoverConst.SPACE+text);
                    if ((i + 1) < constellationYear.getCareer().size()) {
                        content.append("\n");
                    }
                }
            }
            txtWork.setText(content.toString());
        }
        if(SZValidatorUtil.isValidList(constellationYear.getLove())){
            lltHealth.setVisibility(View.VISIBLE);
            StringBuffer content = new StringBuffer();
            for (int i=0;i< constellationYear.getLove().size();i++ ) {
                String text = constellationYear.getLove().get(i);
                if(SZValidatorUtil.isValidString(text)) {
                    content.append(DiscoverConst.SPACE+text);
                    if ((i + 1) < constellationYear.getLove().size()) {
                        content.append("\n");
                    }
                }
            }
            txtLove.setText(content.toString());
        }
    }

    @Override
    public boolean isShowTitleInit() {
        return false;
    }

    @Override
    public String getTitle() {
        return titleShow;
    }

    public void setTitleShow(String titleShow) {
        this.titleShow = titleShow;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}
