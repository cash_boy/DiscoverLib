package com.cn.shuangzi.discover.adp;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.cn.shuangzi.discover.bean.NativeADModel;
import com.cn.shuangzi.discover.bean.RandomBean;
import com.cn.shuangzi.discover.common.NativeADConst;
import com.cn.shuangzi.discover.common.OnNativeADClickListener;
import com.cn.shuangzi.util.SZToast;
import com.cn.shuangzi.util.SZUtil;
import com.cn.shuangzi.view.AlertWidget;

import java.util.ArrayList;
import java.util.List;

import cc.shinichi.library.view.listener.OnBigImagePageChangeListener;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by CN.
 */

public abstract class BaseJokeAdp extends BaseMultiItemQuickAdapter<MultiItemEntity, BaseViewHolder> {
    private OnNativeADClickListener onNativeADClickListener;
    protected Activity activity;
    private AlertWidget txtAlertWidget;
    public BaseJokeAdp(final Activity activity, final List<MultiItemEntity> multiItemEntityList, final OnBigImagePageChangeListener onBigImagePageChangeListener, OnNativeADClickListener onNativeADClickListener, final boolean isPicture) {
        super(multiItemEntityList);
        this.activity = activity;
        addItemTypes();
        this.onNativeADClickListener = onNativeADClickListener;
        setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (multiItemEntityList.get(position - 2) instanceof NativeADModel) {
                    if (BaseJokeAdp.this.onNativeADClickListener != null) {
                        BaseJokeAdp.this.onNativeADClickListener.onNativeADClick((NativeADModel) multiItemEntityList.get(position - 2), position - 2);
                    }
                } else {
                    if (isPicture) {
                        try {
                            List<String> urlList = new ArrayList<>();
                            for (MultiItemEntity multiItemEntity : multiItemEntityList) {
                                if (multiItemEntity instanceof RandomBean.ResultBean) {
                                    RandomBean.ResultBean resultBean = (RandomBean.ResultBean) multiItemEntity;
                                    urlList.add(resultBean.url);
                                }
                            }
                            SZUtil.showBigImg(activity, position - 2, urlList, onBigImagePageChangeListener, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        });
        if(!isPicture) {
            setOnItemLongClickListener(new OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(BaseQuickAdapter adapter, View view, int position) {
                    MultiItemEntity multiItemEntity = multiItemEntityList.get(position - 2);
                    if (multiItemEntity instanceof RandomBean.ResultBean) {
                        final RandomBean.ResultBean resultBean = (RandomBean.ResultBean) multiItemEntity;
                        if(txtAlertWidget!=null&&txtAlertWidget.isDialogShow()){
                            txtAlertWidget.close();
                        }
                        txtAlertWidget = new AlertWidget(activity);
                        String[] menus = {"复制","分享"};
                        txtAlertWidget.setMenu(menus, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which){
                                    case 0:
                                        copyClipboard(activity,resultBean.content);
                                        SZToast.success("已复制到剪贴板！");
                                        break;
                                    case 1:
                                        share(activity,resultBean.content);
                                        break;
                                }
                            }
                        });
                        txtAlertWidget.show();
                    }
                    return false;
                }
            });
        }
    }
    public void share(Context context, String text){
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.setType("text/plain");
        context.startActivity(Intent.createChooser(shareIntent, "分享到"));
    }
    public void copyClipboard(Context context, String text) {
        ClipboardManager myClipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
        ClipData myClip = ClipData.newPlainText("text", text);
        myClipboard.setPrimaryClip(myClip);
    }
    protected void loadGDTView(BaseViewHolder helper, MultiItemEntity item) {
    }

    protected void loadTTCommonView(BaseViewHolder helper, MultiItemEntity item) {
    }

    @Override
    public int getItemViewType(int position) {
        if (mData != null && mData.size() > 0) {
            MultiItemEntity multiItemEntity = mData.get(position);
            if (multiItemEntity instanceof NativeADModel) {
                NativeADModel nativeADModel = (NativeADModel) multiItemEntity;
                return nativeADModel.getItemType();
            } else if (multiItemEntity instanceof RandomBean.ResultBean) {
                return NativeADConst.TYPE_DATA;
            }
        }
        return super.getItemViewType(position);
    }

    public abstract void loadTTView(BaseViewHolder helper, MultiItemEntity item);

    public abstract void addItemTypes();
}
