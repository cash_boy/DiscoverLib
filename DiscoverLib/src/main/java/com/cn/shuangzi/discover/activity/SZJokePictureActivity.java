package com.cn.shuangzi.discover.activity;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.view.BaseJokeView;
import com.cn.shuangzi.discover.view.PictureJokeView;
import com.cn.shuangzi.discover.view.TextJokeView;

/**
 * Created by CN.
 */

public abstract class SZJokePictureActivity extends SZBaseActivity implements SZInterfaceActivity, BaseJokeView.OnLoadListener {
    private PictureJokeView pictureJokeView;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_joke_picture;
    }

    @Override
    protected void onBindChildViews() {
        pictureJokeView = findViewById(R.id.pictureJokeView);
    }

    @Override
    protected void onBindChildListeners() {
        pictureJokeView.setOnLoadListener(this);
    }

    @Override
    protected void onChildViewCreated() {
        setTitleTxt(R.string.txt_joke_picture);
        showBackImgLeft(getBackImgLeft());
        pictureJokeView.loadData(this);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        if(!isRefresh) {
            showBar();
        }
        pictureJokeView.loadData(getActivity());
    }

    @Override
    public void onSuccess() {
        isShowContent(true);
    }

    @Override
    public void onError(boolean isLoadMore) {
        if(!isLoadMore) {
            isShowError(true);
        }
    }
}
