package com.cn.shuangzi.discover.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class DreamSearchInfo implements Serializable{
    private String name;
    private String value;

    public DreamSearchInfo(String name, String value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
