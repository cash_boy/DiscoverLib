package com.cn.shuangzi.discover.adp;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.ConstellationInfo;

import java.util.List;

/**
 * Created by CN.
 */

public class ConstellationAdp extends BaseQuickAdapter<ConstellationInfo,BaseViewHolder>{
    public ConstellationAdp(@Nullable List<ConstellationInfo> data) {
        super(R.layout.adp_constellation, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, ConstellationInfo item) {
        helper.setText(R.id.txtName,item.getName());
        helper.setText(R.id.txtDate,item.getDate());
        helper.setImageResource(R.id.imgConstellation,item.getImgRes());
    }
}
