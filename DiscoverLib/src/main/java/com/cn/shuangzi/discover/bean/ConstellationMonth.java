package com.cn.shuangzi.discover.bean;

/**
 * Created by CN on 2017-12-21.
 */

public class ConstellationMonth {
    private String date;
    private String name;
    private int month;
    private String all;
    private String health;
    private String love;
    private String money;
    private String work;
    private String happyMagic;
//    date : "2017年12月"
//    name : "摩羯座"
//    month : 12
//    all : "这个月，摩羯们心情比较舒畅，睡眠良好，可能去参加一些社群活动或者是应酬。与家人有很多私下的沟通，或者是学习身心灵知识。水星逆行期间，与领导、长辈的私下沟通，需要多加注意细节。电子数据也要备份。 "
//    health : "睡眠良好，身体健康。"
//    love : "有伴的人，注意一些潜在的矛盾。单身的人，这个月低调许多，可以学习身心灵的知识或者宗教。 "
//    money : "财运平稳，事业上的付出越多，回报越大。 "
//    work : "工作运势平稳，沟通避免出现差错。月末，将进入新的阶段。 "
//    happyMagic : ""
//    resultcode : "200"
//    error_code : 0

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public int getMonth() {
        return month;
    }

    public String getAll() {
        return all;
    }

    public String getHealth() {
        return health;
    }

    public String getLove() {
        return love;
    }

    public String getMoney() {
        return money;
    }

    public String getWork() {
        return work;
    }

    public String getHappyMagic() {
        return happyMagic;
    }
}
