package com.cn.shuangzi.discover.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class ConstellationInfo implements Serializable{
    private String name;
    private String date;
    private String type;
    private int imgRes;

    public ConstellationInfo(String name, String date, String type, int imgRes) {
        this.name = name;
        this.date = date;
        this.type = type;
        this.imgRes = imgRes;
    }

    public ConstellationInfo(String name, String date, int imgRes) {
        this.name = name;
        this.date = date;
        this.imgRes = imgRes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }
}
