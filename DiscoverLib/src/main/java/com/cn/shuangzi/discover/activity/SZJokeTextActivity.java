package com.cn.shuangzi.discover.activity;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.view.BaseJokeView;
import com.cn.shuangzi.discover.view.TextJokeView;

/**
 * Created by CN.
 */

public abstract class SZJokeTextActivity extends SZBaseActivity implements SZInterfaceActivity, BaseJokeView.OnLoadListener {
    private TextJokeView textJokeView;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_joke_text;
    }

    @Override
    protected void onBindChildViews() {
        textJokeView = findViewById(R.id.textJokeView);
    }

    @Override
    protected void onBindChildListeners() {
        textJokeView.setOnLoadListener(this);

    }

    @Override
    protected void onChildViewCreated() {
        setTitleTxt(R.string.txt_joke_text);
        showBackImgLeft(getBackImgLeft());
        textJokeView.loadData(this);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        if(!isRefresh) {
            showBar();
        }
        textJokeView.loadData(getActivity());
    }

    @Override
    public void onSuccess() {
        isShowContent(true);
    }

    @Override
    public void onError(boolean isLoadMore) {
        if(!isLoadMore) {
            isShowError(true);
        }
    }
}
