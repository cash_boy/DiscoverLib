package com.cn.shuangzi.discover.adp;

import android.app.Activity;
import android.widget.ImageView;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.RandomBean;
import com.cn.shuangzi.discover.common.NativeADConst;
import com.cn.shuangzi.discover.common.OnNativeADClickListener;
import com.cn.shuangzi.util.SZImageLoader;

import java.util.List;

import cc.shinichi.library.view.listener.OnBigImagePageChangeListener;

public class RandomPictureJokeAdp extends BaseJokeAdp {
    private SZImageLoader imageLoader;
    public RandomPictureJokeAdp(Activity activity, List<MultiItemEntity> multiItemEntityList, OnBigImagePageChangeListener onBigImagePageChangeListener, OnNativeADClickListener onNativeADClickListener) {
        super(activity, multiItemEntityList,onBigImagePageChangeListener, onNativeADClickListener, true);
        imageLoader = new SZImageLoader(activity);
    }

    @Override
    public void addItemTypes() {
        addItemType(NativeADConst.TYPE_DATA, R.layout.adp_picture_joke);
    }

    @Override
    protected void convert(BaseViewHolder helper, MultiItemEntity item) {
        switch (helper.getItemViewType()) {
            case NativeADConst.TYPE_DATA:
                loadDataView(helper, item);
                break;
            case NativeADConst.TYPE_GDT:
                loadGDTView(helper, item);
                break;
            case NativeADConst.TYPE_TT:
                loadTTCommonView(helper, item);
                break;
        }
    }

    private void loadDataView(BaseViewHolder helper, MultiItemEntity item) {
        RandomBean.ResultBean dataBean = (RandomBean.ResultBean) item;
        ImageView mSdvImageView = helper.getView(R.id.sdv_image_view);
        helper.setText(R.id.content, dataBean.content);
        imageLoader.load(mSdvImageView,dataBean.url,R.mipmap.ic_placeholder);
    }

    @Override
    public void loadTTView(BaseViewHolder helper, MultiItemEntity item) {
    }
}
