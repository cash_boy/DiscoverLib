package com.cn.shuangzi.discover.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.cn.shuangzi.SZBaseFragment;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.ConstellationDay;
import com.cn.shuangzi.discover.common.DiscoverConst;
import com.cn.shuangzi.discover.common.HttpRequest;
import com.cn.shuangzi.discover.view.StarBarView;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by CN on 2017-12-21.
 */

public class ConstellationDayFragment extends SZBaseFragment {
    private String constellationName;
    private String type;
    private TextView txtDate;
    private LinearLayout lltAll;
    private StarBarView starBarViewAll;
    private LinearLayout lltHealth;
    private StarBarView starBarViewHealth;
    private LinearLayout lltLove;
    private StarBarView starBarViewLove;
    private LinearLayout lltWork;
    private StarBarView starBarViewWork;
    private LinearLayout lltMoney;
    private StarBarView starBarViewMoney;
    private LinearLayout lltLuckyNumber;
    private TextView txtLuckyNumber;
    private LinearLayout lltLuckyColor;
    private TextView txtLuckyColor;
    private LinearLayout lltFriend;
    private TextView txtFriendConstellation;
    private LinearLayout lltSummary;
    private TextView txtSummary;
    private String titleShow;
    public static ConstellationDayFragment newInstance(String type, String constellationName) {
        ConstellationDayFragment fragment = new ConstellationDayFragment();
        Bundle args = new Bundle();
        args.putString("name", constellationName);
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected int onGetChildView() {
        return R.layout.fragment_constellation_day;
    }

    @Override
    protected void onBindChildViews() {
        txtDate = findViewById(R.id.txtDate);
        lltAll = findViewById(R.id.lltAll);
        starBarViewAll = findViewById(R.id.starBarViewAll);
        lltHealth = findViewById(R.id.lltHealth);
        starBarViewHealth = findViewById(R.id.starBarViewHealth);
        lltLove = findViewById(R.id.lltLove);
        starBarViewLove = findViewById(R.id.starBarViewLove);
        lltWork = findViewById(R.id.lltWork);
        starBarViewWork = findViewById(R.id.starBarViewWork);
        lltMoney = findViewById(R.id.lltMoney);
        starBarViewMoney = findViewById(R.id.starBarViewMoney);
        lltLuckyNumber = findViewById(R.id.lltLuckyNumber);
        txtLuckyNumber = findViewById(R.id.txtLuckyNumber);
        lltLuckyColor = findViewById(R.id.lltLuckyColor);
        txtLuckyColor = findViewById(R.id.txtLuckyColor);
        lltFriend = findViewById(R.id.lltFriend);
        txtFriendConstellation = findViewById(R.id.txtFriendConstellation);
        lltSummary = findViewById(R.id.lltSummary);
        txtSummary = findViewById(R.id.txtSummary);
    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        initParam();
    }

    @Override
    public void onVisible() {
        super.onVisible();
        if(isFirstLoad()){
            initParam();
            setFirstLoad(false);
            onReloadData(false);
        }
    }
    private void initParam(){
        if(constellationName == null) {
            constellationName = getArguments().getString("name");
            type = getArguments().getString("type");
        }
        if(constellationName == null){
            getActivity().finish();
            return;
        }
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
        showBar();
        Map<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("consName", constellationName);
        HttpRequest.request(DiscoverConst.URL_CONSTELLATION, params, getRequestTag(), new HttpRequest
                .HttpResponseSimpleListener() {
            @Override
            public void onNetError(String url, int errorCode) {
                isShowError(true);
            }

            @Override
            public void onSuccess(String url, String responseData) {
                isShowContent(true);
                ConstellationDay constellationDay = new Gson().fromJson(responseData,ConstellationDay.class);
                initView(constellationDay);
            }
        });
    }
    private void initView(ConstellationDay constellationDay){
//        if(SZValidatorUtil.isValidString(constellationDay.getDatetime())){
//            txtDate.setVisibility(View.VISIBLE);
//            txtDate.setText(constellationDay.getDatetime());
//        }
        if(constellationDay.getAll()>0){
            lltAll.setVisibility(View.VISIBLE);
            starBarViewAll.setStarRating(constellationDay.getAll());
        }
        if(constellationDay.getHealth()>0){
            lltHealth.setVisibility(View.VISIBLE);
            starBarViewHealth.setStarRating(constellationDay.getHealth());
        }
        if(constellationDay.getLove()>0){
            lltLove.setVisibility(View.VISIBLE);
            starBarViewLove.setStarRating(constellationDay.getLove());
        }
        if(constellationDay.getWork()>0){
            lltWork.setVisibility(View.VISIBLE);
            starBarViewWork.setStarRating(constellationDay.getWork());
        }
        if(constellationDay.getMoney()>0){
            lltMoney.setVisibility(View.VISIBLE);
            starBarViewMoney.setStarRating(constellationDay.getMoney());
        }
        if(SZValidatorUtil.isValidString(constellationDay.getColor())){
            lltLuckyColor.setVisibility(View.VISIBLE);
            txtLuckyColor.setText(constellationDay.getColor());
        }
        if(SZValidatorUtil.isValidString(constellationDay.getNumber())){
            lltLuckyNumber.setVisibility(View.VISIBLE);
            txtLuckyNumber.setText(constellationDay.getNumber());
        }
        if(SZValidatorUtil.isValidString(constellationDay.getQFriend())){
            lltFriend.setVisibility(View.VISIBLE);
            txtFriendConstellation.setText(constellationDay.getQFriend());
        }
        if(SZValidatorUtil.isValidString(constellationDay.getSummary())){
            lltSummary.setVisibility(View.VISIBLE);
            txtSummary.setText(constellationDay.getSummary());
        }
    }

    @Override
    public boolean isShowTitleInit() {
        return false;
    }

    @Override
    public String getTitle() {
        return titleShow;
    }

    public void setTitleShow(String titleShow) {
        this.titleShow = titleShow;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}
