package com.cn.shuangzi.discover;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.cn.shuangzi.SZManager;
import com.cn.shuangzi.discover.bean.RandomBean;
import com.cn.shuangzi.discover.common.NativeADConst;


public class ImageDetailActivity extends AppCompatActivity implements View.OnClickListener {
    private RandomBean.ResultBean resultBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_detail);
//        Util.keepFullScreen(this);
        resultBean = (RandomBean.ResultBean) getIntent().getSerializableExtra(NativeADConst.URI);
        if (resultBean == null || resultBean.url == null || resultBean.url.trim().equals("")) {
            finish();
            return;
        }
        findViewById(R.id.imgLeft).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.imgLeft) {
            onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            SZManager.getInstance().onUMResume(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            SZManager.getInstance().onUMPause(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
