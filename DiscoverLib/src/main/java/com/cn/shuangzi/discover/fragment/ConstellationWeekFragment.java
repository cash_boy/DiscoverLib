package com.cn.shuangzi.discover.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cn.shuangzi.SZBaseFragment;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.ConstellationDay;
import com.cn.shuangzi.discover.bean.ConstellationWeek;
import com.cn.shuangzi.discover.common.DiscoverConst;
import com.cn.shuangzi.discover.common.HttpRequest;
import com.cn.shuangzi.discover.view.StarBarView;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by CN on 2017-12-21.
 */

public class ConstellationWeekFragment extends SZBaseFragment {
    private String constellationName;
    private String type;
    private TextView txtDate;
    private LinearLayout lltHealth;
    private TextView txtHealth;
    private LinearLayout lltLove;
    private TextView txtLove;
    private LinearLayout lltWork;
    private TextView txtWork;
    private LinearLayout lltMoney;
    private TextView txtMoney;
    private String titleShow;
    public static ConstellationWeekFragment newInstance(String type, String constellationName) {
        ConstellationWeekFragment fragment = new ConstellationWeekFragment();
        Bundle args = new Bundle();
        args.putString("name", constellationName);
        args.putString("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int onGetChildView() {
        return R.layout.fragment_constellation_week;
    }

    @Override
    protected void onBindChildViews() {
        txtDate = findViewById(R.id.txtDate);
        txtHealth = findViewById(R.id.txtHealth);
        lltHealth = findViewById(R.id.lltHealth);
        txtLove = findViewById(R.id.txtLove);
        lltLove = findViewById(R.id.lltLove);
        txtWork = findViewById(R.id.txtWork);
        lltWork = findViewById(R.id.lltWork);
        txtMoney = findViewById(R.id.txtMoney);
        lltMoney = findViewById(R.id.lltMoney);
    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        initParam();
    }

    @Override
    public void onVisible() {
        super.onVisible();
        if(isFirstLoad()){
            initParam();
            setFirstLoad(false);
            onReloadData(false);
        }
    }
    private void initParam(){
        if(constellationName == null) {
            constellationName = getArguments().getString("name");
            type = getArguments().getString("type");
        }
        if(constellationName == null){
            getActivity().finish();
            return;
        }
    }
    @Override
    protected void onReloadData(boolean isRefresh) {
        showBar();
        Map<String, String> params = new HashMap<>();
        params.put("type", type);
        params.put("consName", constellationName);
        HttpRequest.request(DiscoverConst.URL_CONSTELLATION, params, getRequestTag(), new HttpRequest
                .HttpResponseSimpleListener() {
            @Override
            public void onNetError(String url, int errorCode) {
                isShowError(true);
            }

            @Override
            public void onSuccess(String url, String responseData) {
                isShowContent(true);
                ConstellationWeek constellationWeek = new Gson().fromJson(responseData,ConstellationWeek.class);
                initView(constellationWeek);
            }
        });
    }
    private void initView(ConstellationWeek constellationWeek){
//        if(SZValidatorUtil.isValidString(constellationWeek.getDate())){
//            txtDate.setVisibility(View.VISIBLE);
//            txtDate.setText(constellationWeek.getDate());
//        }
        if(SZValidatorUtil.isValidString(constellationWeek.getHealth())){
            lltHealth.setVisibility(View.VISIBLE);
            txtHealth.setText(DiscoverConst.SPACE+constellationWeek.getHealth());
        }
        if(SZValidatorUtil.isValidString(constellationWeek.getLove())){
            lltLove.setVisibility(View.VISIBLE);
            txtLove.setText(DiscoverConst.SPACE+constellationWeek.getLove());
        }
        if(SZValidatorUtil.isValidString(constellationWeek.getMoney())){
            lltMoney.setVisibility(View.VISIBLE);
            txtMoney.setText(DiscoverConst.SPACE+constellationWeek.getMoney());
        }
        if(SZValidatorUtil.isValidString(constellationWeek.getWork())){
            lltWork.setVisibility(View.VISIBLE);
            txtWork.setText(DiscoverConst.SPACE+constellationWeek.getWork());
        }
    }

    @Override
    public boolean isShowTitleInit() {
        return false;
    }

    @Override
    public String getTitle() {
        return titleShow;
    }

    public void setTitleShow(String titleShow) {
        this.titleShow = titleShow;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}
