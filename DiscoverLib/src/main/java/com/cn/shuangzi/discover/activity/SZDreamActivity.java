package com.cn.shuangzi.discover.activity;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.adp.DreamCateAdp;
import com.cn.shuangzi.discover.bean.DreamCate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CN.
 */

public abstract class SZDreamActivity extends SZBaseActivity implements SZInterfaceActivity{
    private RecyclerView recyclerView;
    private DreamCateAdp dreamCateAdp;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_dream;
    }

    @Override
    protected void onBindChildViews() {
        recyclerView = findViewById(R.id.recyclerView);
    }

    @Override
    protected void onBindChildListeners() {

        findViewById(R.id.rltSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(getSearchActivity());
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        showBackImgLeft(getBackImgLeft());
        setTitleTxt(R.string.txt_interpret_dream);
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
        final List<DreamCate> dreamCateList = new ArrayList<>();
        dreamCateList.add(new DreamCate("人物类",R.mipmap.ic_person));
        dreamCateList.add(new DreamCate("动物类",R.mipmap.ic_animal));
        dreamCateList.add(new DreamCate("植物类",R.mipmap.ic_plant));
        dreamCateList.add(new DreamCate("情感类",R.mipmap.ic_qinggan));
        dreamCateList.add(new DreamCate("鬼神类",R.mipmap.ic_guishen));
        dreamCateList.add(new DreamCate("建筑类",R.mipmap.ic_jianzhu));
        dreamCateList.add(new DreamCate("生活类",R.mipmap.ic_life));
        dreamCateAdp = new DreamCateAdp(this,dreamCateList);
        dreamCateAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                startActivity(dreamCateList.get(position).getName(),getDreamCateActivity());
            }
        });
        recyclerView.setAdapter(dreamCateAdp);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }
    public abstract Class getDreamCateActivity();
    public abstract Class getSearchActivity();
}
