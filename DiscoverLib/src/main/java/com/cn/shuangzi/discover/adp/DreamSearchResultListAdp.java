package com.cn.shuangzi.discover.adp;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.cn.shuangzi.adp.SZBaseAdp;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.DreamInfo;
import com.cn.shuangzi.util.SZValidatorUtil;

import java.util.List;

/**
 * Created by CN.
 */

public class DreamSearchResultListAdp extends SZBaseAdp<DreamInfo> {
    public DreamSearchResultListAdp(Context context, @Nullable List<DreamInfo> data) {
        super(context, R.layout.adp_dream_search_result, data, R.string.txt_empty_search);
    }

    @Override
    protected void convert(BaseViewHolder helper, DreamInfo item) {
        helper.setText(R.id.txtTitle, item.getTitle());
        if (SZValidatorUtil.isValidString(item.getDes())) {
            helper.setText(R.id.txtContent, item.getDes());
            helper.setGone(R.id.txtContent, true);
        } else {
            helper.setGone(R.id.txtContent, false);
        }
    }
}
