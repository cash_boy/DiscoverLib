package com.cn.shuangzi.discover.bean;

import java.util.List;

/**
 * Created by CN on 2017-12-21.
 */

public class ConstellationYear {
    private String name;
    private String date;
    private String year;
    private Mima mima;
    private List<String> career;
    private List<String> love;
    private List<String> health;
    private List<String> finance;
    private String luckeyStone;
    private String future;
    public class Mima{
        private String info;
        private List<String> text;

        public String getInfo() {
            return info;
        }

        public List<String> getText() {
            return text;
        }
    }
//    name : "摩羯座"
//    date : "2017年"
//    year : 2017
//    mima
//    info : "隐忍数年 终成大器"
//    text
//0 : "对大多数摩羯座而言，未来一年将是数年隐忍之后，终究修成神功的一年。在明年，摩羯座的命主星土星还将在自身的因果宫内停留将近一年，这意味着虽然成功在望，但大多数时候你们还是要继续修炼忍者神功，压住自己内心的疑问和迷茫，坚持将手头事情做好，不让心情影响现有的基本盘。但另一方面，木星会在事业宫照耀，也让你们的能见度和成功率都大大提升，也许你还是觉得大材小用或是当下做的并不是自己最理想的工作，但结果却非常喜人，也会给后续带来更多多元发展的机会。土天冲也会持续影响你的家宅宫，部分摩羯座会购置房产或是转换地方（搬到另外一个城市乃至国家），另一些摩羯座则会发现自己曾经的认知再度经历巨变，只有改变现有的内在状态，才可能实现下一步。"
//    career
//0 : "当下木星正在摩羯座的事业宫内停留，这也意味着摩羯座在事业方面拥有很多过往难以想象的好运，习惯凡事只要拥有侥幸心理就会失败的你们可能会不太适应这种意料之外的好运气，但这其实也是对你们多年以来兢兢业业的回报。上班族将会很容易就得到合作方的配合，涉及沟通方面的问题也会尤其顺遂，或是原本轮不到自己的好事从天而降落，只要付出努力就会很容易获得上级嘉奖乃至各种隐性的奖金奖励也绝对不少。管理者相对容易在行业竞争中胜出，得到条件不错的合约，即便与自己想做的业务有所出入，却也算是歪打正着创出一片天地。整年来看，3、4、5月是较为容易出现差错或是被个人事务拖累工作状态的时段，进入夏天之后，很多重要的事情都会开始酝酿，直到8月底完成重要的跳槽、升职和业务。此后，摩羯们只要顺应时局，将手头业务做好，等待年底的升级时刻即可。"
//    love
//0 : "2017年你们会把大量精力都会灌注在事业发展之上，因此很多感情事件和发展轴线都与事业息息相关。单身者会在私人场合有机会碰到一些似是而非的桃花，但多数仅限于短暂接触或是聊骚阶段，很难真正正式发展。反而是工作中遇到的合作方可能因为你个人出色的能力而倍加欣赏，并有进一步互动，但基本要进入夏天之后才会有相对清晰的对象浮出水面，并有可能在8月底确定关系走向。有伴个人则在前三个季度感情运相对平稳，对方会一直给予你生活和事业上的支持，尤其帮助你度过最难熬的一段时光，双方也会因为沟通而获得更好的相互理解和共同进步。10月10日木星移位天蝎座，这让单身人士有机会进入一个全新朋友圈，也因此多了很多露水姻缘的机会，桃花太旺反而很难稳定下来。有伴者若能约束自身，与另一半共同参加各类活动，也是提升感情运的好机会。"
//    health
//0 : "今天身体不太好，要特别留意，同时情绪上的管理要做好，不要动不动就发脾气。"
//    finance
//0 : "未来一年，摩羯座的财运会持续稳步上扬，各种额外开销都会逐步降低，意外收入却会同时增加，不论是上班族、管理者还是自雇业者都会因为事业运的上扬而明显提升正财收入。对上班族而言，你只要做好日常工作，就会很容易被领导看到，虽然未必能够从明显上涨薪，但福利、红包乃至隐性收入却会明显增加，让你们不用再时刻担心荷包问题，甚是幸福。管理者则很容易因为合理压缩企业冗余开支的同时获得利润较高的营业收入而明显提升利润水平。自雇业者会因为个人出色的业务能力得到很多行业内重要订单，甚至一些临时出问题的事情也很容易找到头上，因为半路接盘而名利双收，总体而言，只要肯做就绝对不会缺钱。但要注意，新一年内投资运只算一般，尤其上半年多为浮盈，年后更有损失风险，不如退守等待下半年再出手。 "
//    luckeyStone : "紫黄晶"
//    future : ""
//    resultcode : "200"
//    error_code : 0

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getYear() {
        return year;
    }

    public Mima getMima() {
        return mima;
    }

    public List<String> getCareer() {
        return career;
    }

    public List<String> getLove() {
        return love;
    }

    public List<String> getHealth() {
        return health;
    }

    public List<String> getFinance() {
        return finance;
    }

    public String getLuckeyStone() {
        return luckeyStone;
    }

    public String getFuture() {
        return future;
    }
}
