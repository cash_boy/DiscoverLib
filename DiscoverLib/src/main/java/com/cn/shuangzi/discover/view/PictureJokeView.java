package com.cn.shuangzi.discover.view;

import android.content.Context;
import android.util.AttributeSet;

import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.cn.shuangzi.discover.adp.RandomPictureJokeAdp;
import com.cn.shuangzi.discover.bean.NativeADModel;
import com.cn.shuangzi.discover.common.NativeADConst;
import com.cn.shuangzi.discover.common.OnNativeADClickListener;
import com.cn.shuangzi.util.SZUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cc.shinichi.library.view.listener.OnBigImagePageChangeListener;

/**
 * Created by CN.
 */

public class PictureJokeView extends BaseJokeView implements OnNativeADClickListener, OnBigImagePageChangeListener {
    private RandomPictureJokeAdp pictureJokeAdp;

    public PictureJokeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PictureJokeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public int getADImgWidth() {
        return getWidth() - SZUtil.dip2px(30);
    }

    @Override
    public int getADImgHeight() {
        return SZUtil.dip2px(150);
    }

    @Override
    public Map<String, String> getParams() {
        Map<String, String> map = new HashMap<>();
        map.put("key", NativeADConst.APPKEY_JOKE);
        map.put("type", "pic");
        return map;
    }

    @Override
    public String getRequestTag() {
        return "PictureJokeView";
    }

    @Override
    public void setAdapterData(List<MultiItemEntity> multiItemEntityList) {
        if(pictureJokeAdp == null){
            pictureJokeAdp = new RandomPictureJokeAdp(activity,multiItemEntityList,this,this);
            recyclerView.setIAdapter(pictureJokeAdp);
        }else{
            pictureJokeAdp.notifyDataSetChanged();
        }
    }

    @Override
    public void onNativeADClick(NativeADModel nativeADModel, int position) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        recyclerView.scrollToPosition(position+2);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
