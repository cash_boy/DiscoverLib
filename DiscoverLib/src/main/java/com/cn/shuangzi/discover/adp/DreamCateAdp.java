package com.cn.shuangzi.discover.adp;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.cn.shuangzi.adp.SZBaseAdp;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.DreamCate;

import java.util.List;

/**
 * Created by CN.
 */

public class DreamCateAdp extends SZBaseAdp<DreamCate> {
    private boolean isShowIcon = true;
    public DreamCateAdp(Context context, @Nullable List<DreamCate> data) {
        super(context, R.layout.adp_dream_cate, data);
    }
    public DreamCateAdp(Context context, @Nullable List<DreamCate> data,boolean isShowIcon) {
        super(context, R.layout.adp_dream_cate, data);
        this.isShowIcon = isShowIcon;
    }

    @Override
    protected void convert(BaseViewHolder helper, DreamCate item) {
        helper.setText(R.id.txtName,item.getName());
        if(isShowIcon) {
            helper.setImageResource(R.id.imgIcon, item.getResId());
        }else{
            helper.setGone(R.id.imgIcon,false);
        }
    }
}
