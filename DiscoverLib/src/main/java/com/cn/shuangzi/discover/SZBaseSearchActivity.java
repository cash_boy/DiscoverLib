package com.cn.shuangzi.discover;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.cn.shuangzi.SZBaseActivity;

/**
 * Created by CN.
 */

public abstract class SZBaseSearchActivity extends SZBaseActivity implements View.OnKeyListener {
    private EditText editTextSearch;

    protected void initSearch(EditText editText) {
        this.editTextSearch = editText;
        this.editTextSearch.setOnKeyListener(this);
        this.editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void afterTextChanged(Editable editable) {
                onTextChange(editTextSearch.getText().toString().trim());
            }
        });
    }

    public abstract void onSearch(String search);
    public abstract void onTextChange(String content);

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_UP) {   // 忽略其它事件
            return false;
        }
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                String search = editTextSearch.getText().toString();
                onSearch(search);
                return true;
            default:
                return false;
        }
    }

}
