package com.cn.shuangzi.discover.activity;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.adp.DreamSearchCateAdp;
import com.cn.shuangzi.discover.bean.DreamCate;
import com.cn.shuangzi.discover.common.DiscoverConst;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by CN.
 */

public abstract class SZDreamCateActivity extends SZBaseActivity implements SZInterfaceActivity{
    private RecyclerView recyclerView;
    private DreamSearchCateAdp dreamCateAdp;
    private String type;
    @Override
    protected int onGetChildView() {
        return R.layout.activity_dream;
    }

    @Override
    protected void onBindChildViews() {
        recyclerView = findViewById(R.id.recyclerView);
    }

    @Override
    protected void onBindChildListeners() {
        findViewById(R.id.rltSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(getSearchActivity());
            }
        });
    }

    @Override
    protected void onChildViewCreated() {
        type = getStringExtra();
        if(type == null){
            finish();
            return;
        }
        showBackImgLeft(getBackImgLeft());
        setTitleTxt(type);
        recyclerView.setLayoutManager(new GridLayoutManager(this,3));
        final List<String> dreamCateValueList = DiscoverConst.DREAM_TYPE_LIST_MAP.get(type);
        List<DreamCate> dreamCateList = new ArrayList<>();
        for (String dream : dreamCateValueList){
            dreamCateList.add(new DreamCate(dream));
        }
        dreamCateAdp = new DreamSearchCateAdp(this,dreamCateList);
        dreamCateAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                startActivity(dreamCateValueList.get(position),getSearchResultListActivity());
            }
        });
        recyclerView.setAdapter(dreamCateAdp);
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }
    public abstract Class getSearchResultListActivity();
    public abstract Class getSearchActivity();
}
