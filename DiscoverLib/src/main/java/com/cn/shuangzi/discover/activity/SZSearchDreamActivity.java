package com.cn.shuangzi.discover.activity;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.SZBaseSearchActivity;
import com.cn.shuangzi.discover.adp.DreamSearchResultListAdp;
import com.cn.shuangzi.discover.bean.DreamInfo;
import com.cn.shuangzi.discover.common.DiscoverConst;
import com.cn.shuangzi.discover.common.HttpRequest;
import com.cn.shuangzi.util.SZIRecyclerViewUtil;
import com.cn.shuangzi.util.SZValidatorUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CN.
 */

public abstract class SZSearchDreamActivity extends SZBaseSearchActivity implements View.OnClickListener {
    private RecyclerView recyclerView;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_search_dream;
    }

    @Override
    protected void onBindChildViews() {
        recyclerView = findViewById(R.id.recyclerView);
    }

    @Override
    protected void onBindChildListeners() {

    }

    @Override
    protected void onChildViewCreated() {
        initSearch((EditText) findViewById(R.id.edtTxtSearch));
    }

    @Override
    protected void onReloadData(boolean isRefresh) {
    }

    @Override
    public void onSearch(String search) {
        if (!SZValidatorUtil.isValidString(search)) {
            recyclerView.setVisibility(View.GONE);
            return;
        }
        showBar();
        Map<String, String> params = new HashMap<>();
        params.put("q", search);
        HttpRequest.request(DiscoverConst.URL_DREAM_SEARCH, params, getRequestTag(), new HttpRequest.HttpResponseSimpleListener() {
            @Override
            public void onNetError(String url, int errorCode) {
                closeBar();
            }

            @Override
            public void onSuccess(String url, String responseData) {
                closeBar();
                recyclerView.setVisibility(View.VISIBLE);
                final List<DreamInfo> dreamInfoList = new Gson().fromJson(responseData, new TypeToken<List<DreamInfo>>() {
                }.getType());
                DreamSearchResultListAdp dreamSearchResultListAdp = new DreamSearchResultListAdp(getActivity(),dreamInfoList);
                dreamSearchResultListAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        startActivity(dreamInfoList.get(position).getId(), getDreamDetailActivity());
                    }
                });
                SZIRecyclerViewUtil.setVerticalLinearLayoutManager(getActivity(), recyclerView);
                recyclerView.setAdapter(dreamSearchResultListAdp);
            }
        });
    }

    @Override
    public void onTextChange(String content) {
        if (!SZValidatorUtil.isValidString(content)) {
            recyclerView.setVisibility(View.GONE);
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("q", content);
        HttpRequest.request(DiscoverConst.URL_DREAM_SEARCH, params, getRequestTag(), new HttpRequest.HttpResponseSimpleListener() {
            @Override
            public void onNetError(String url, int errorCode) {
            }

            @Override
            public void onSuccess(String url, String responseData) {
                recyclerView.setVisibility(View.VISIBLE);
                final List<DreamInfo> dreamInfoList = new Gson().fromJson(responseData, new TypeToken<List<DreamInfo>>() {
                }.getType());
                DreamSearchResultListAdp dreamSearchResultListAdp = new DreamSearchResultListAdp(getActivity(),dreamInfoList);
                dreamSearchResultListAdp.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        startActivity(dreamInfoList.get(position).getId(), getDreamDetailActivity());
                    }
                });
                SZIRecyclerViewUtil.setVerticalLinearLayoutManager(getActivity(), recyclerView);
                recyclerView.setAdapter(dreamSearchResultListAdp);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.txtCancel) {
            finish();
        }
    }

    @Override
    public boolean isShowTitleInit() {
        return false;
    }

    public abstract Class getDreamDetailActivity();
}
