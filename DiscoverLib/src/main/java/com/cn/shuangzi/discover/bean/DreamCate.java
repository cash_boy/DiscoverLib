package com.cn.shuangzi.discover.bean;

import java.io.Serializable;

/**
 * Created by CN.
 */

public class DreamCate implements Serializable{
    private String id;
    private String name;
    private String fid;
    private int resId;

    public DreamCate(String name) {
        this.name = name;
    }

    public DreamCate(String name, int resId) {
        this.name = name;
        this.resId = resId;
    }

    public DreamCate(String id, String name, String fid, int resId) {
        this.id = id;
        this.name = name;
        this.fid = fid;
        this.resId = resId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }
}
