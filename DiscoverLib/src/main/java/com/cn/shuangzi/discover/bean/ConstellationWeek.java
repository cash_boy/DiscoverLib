package com.cn.shuangzi.discover.bean;

/**
 * Created by CN on 2017-12-21.
 */

public class ConstellationWeek {
    private String name;
    private int weekth;
    private String date;
    private String health;
    private String job;
    private String love;
    private String money;
    private String work;
//     : "摩羯座"
//     : 51
//    date : "2017年12月17日-2017年12月23日"
//    health : "健康：注意手臂，肩膀的问题。 "
//    job : "求职：求职的关键在于，机会很多，但都有不易察觉的，看你是否能够抓得住。 "
//    love : "恋情：单身的人，多参加一些社会活动，有机会结识你的TA。 "
//    money : "财运：财运平常。无大的起落。 "
//    work : "工作：本周更适合做外部工作，与社会上的人打交道。 "
//    resultcode : "200"
//    error_code : 0

    public String getName() {
        return name;
    }

    public int getWeekth() {
        return weekth;
    }

    public String getDate() {
        return date;
    }

    public String getHealth() {
        return health;
    }

    public String getJob() {
        return job;
    }

    public String getLove() {
        return love;
    }

    public String getMoney() {
        return money;
    }

    public String getWork() {
        return work;
    }
}
