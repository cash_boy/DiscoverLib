package com.cn.shuangzi.discover.adp;

import android.content.Context;
import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseViewHolder;
import com.cn.shuangzi.adp.SZBaseAdp;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.DreamCate;

import java.util.List;

/**
 * Created by CN.
 */

public class DreamSearchCateAdp extends SZBaseAdp<DreamCate> {
    public DreamSearchCateAdp(Context context, @Nullable List<DreamCate> data) {
        super(context, R.layout.adp_dream_search_cate, data);
    }

    @Override
    protected void convert(BaseViewHolder helper, DreamCate item) {
        helper.setText(R.id.txtName,"梦见" + item.getName());
    }
}
