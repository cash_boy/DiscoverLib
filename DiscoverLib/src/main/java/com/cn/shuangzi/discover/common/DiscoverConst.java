package com.cn.shuangzi.discover.common;

import com.cn.shuangzi.discover.bean.DreamSearchInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CN.
 */

public class DiscoverConst {

    public static final String PARAM_Q_DREAM = "q";
    public static final String PARAM_CONS_NAME = "consName";
    public static final String PARAM_YPTE = "type";
    public static final String TYPE_VALUE_TODAY = "today";
    public static final String TYPE_VALUE_TOMORROW = "tomorrow";
    public static final String TYPE_VALUE_WEEK = "week";
    public static final String TYPE_VALUE_NEXTWEEK = "nextweek";
    public static final String TYPE_VALUE_MONTH = "month";
    public static final String TYPE_VALUE_YEAR = "year";
    public static final String URL_CONSTELLATION = "http://web.juhe.cn:8080/constellation/getAll?key=592e835d90c24978f91478c80a436d1c";
    public static final boolean DEBUG = true;
    //解梦分类
//    public static String URL_DREAM_CLASSIFY = "http://v.juhe.cn/dream/category?key=d7adea3c9abc7c01c9c30bde59574669";
    //关键字搜索
    public static String URL_DREAM_SEARCH_DETAIL = "http://v.juhe" +
            ".cn/dream/queryid?key=57b8d5b0d2a9457bf875774013dd4fee&full=1";
    public static String URL_DREAM_SEARCH = "http://v.juhe.cn/dream/query?key=d7adea3c9abc7c01c9c30bde59574669";
    public static final Map<String,List<String>> DREAM_TYPE_LIST_MAP = new HashMap<>();
    public static final List<String> PERSON_LIST = new ArrayList<>();
    public static final List<String> ANIMAL_LIST = new ArrayList<>();
    public static final List<String> QINGGAN_LIST = new ArrayList<>();
    public static final List<String> ZHIWU_LIST = new ArrayList<>();
    public static final List<String> LIFE_LIST = new ArrayList<>();
    public static final List<String> GUISHEN_LIST = new ArrayList<>();
    public static final List<String> JIANZHU_LIST = new ArrayList<>();

    public static final Map<String,String> CONSTELLATION_DES_MAP = new HashMap<>();
    public static final Map<String,String> CONSTELLATION_SEARCH_MAP = new HashMap<>();
    public static final String SPACE = "        ";
    static {
        CONSTELLATION_DES_MAP.put("白羊座","        白羊座的内心一直是个孩子，保有天真的一面，即使长大了，他还是相信世界上有小精灵的存在。有时白羊座的人相当幼稚，可是他绝对不是不聪明，他的反应可快得很呢！");
        CONSTELLATION_DES_MAP.put("金牛座","        金牛座的性格平稳、有毅力和耐力，勤劳智慧，富有实干精神，他的突出特点就是执着，家庭观念较强，思想趋于保守但又十分大胆，但善于用理财，是个自我要求完美的人。");
        CONSTELLATION_DES_MAP.put("双子座","        双子座的人无拘无束，对外界的事物有永无休止的好奇心。有典型的大城市人气质，生活节奏快，每天有各种各样的活动和安排，弱点是好动和缺乏耐心。");
        CONSTELLATION_DES_MAP.put("巨蟹座","        超群的直觉和敏感是巨蟹座人的主要性格特征，喜欢收集储存东西，对任何事情都不舍不弃。他们充满爱心，感情真挚、坦诚，但性格比较脆弱，不善于在公众面前表现自己。");
        CONSTELLATION_DES_MAP.put("狮子座","        狮子座有着宏伟的理想，总想靠自己的努力成为人上人，你向往高高在上的优越感，也期待被仰慕被崇拜的感觉，有点儿自信有点儿自大。狮子男的大男人气息很重，爱面子，狮子女热情阳光，对朋友讲义气。");
        CONSTELLATION_DES_MAP.put("处女座","        处女座虽然常常被黑，但你还是依然坚持追求自己的完美主义，因为在你看来，生活不能将就，追求的完美更不能将就，有目标才有进步，当然也需要鼓励。处女男的毅力很强，能坚持，处女女的求知欲很强。");
        CONSTELLATION_DES_MAP.put("天秤座","        天秤座常常追求平等、和谐，擅于察言观色，交际能力很强，因此真心朋友不少，因为你也足够真诚，但是最大的缺点就是面对选择总是犹豫不决。天秤男容易在乎自己而忽略别人，天秤女就喜欢被陪伴的感觉。");
        CONSTELLATION_DES_MAP.put("天蝎座","        天蝎座精力旺盛、占有欲极强，对于生活很有目标，不达到目的誓不罢休，复仇心理重，记仇会让自己不顾一切报复曾经伤害过你的人。天蝎男自我主义色彩很强烈，天蝎女的自我保护意识很强，不容易接近。");
        CONSTELLATION_DES_MAP.put("射手座","        射手座崇尚自由，勇敢、果断、独立，身上有一股勇往直前的劲儿，不管有多困难，只要想，就能做，你的毅力和自信是难以击倒的。射手男酷爱自由，讨厌被束缚，射手女性格简单直接，不耍心计，可是任性。");
        CONSTELLATION_DES_MAP.put("摩羯座","        摩羯座是十二星座中最有耐心，为事最小心、也是最善良的星座。他们做事脚踏实地，也比较固执，不达目的是不会放手的。他们的忍耐力也是出奇的强大，同时也非常勤奋。他们心中总是背负着很多的责任感，但往往又很没有安全感，不会完全地相信别人。");
        CONSTELLATION_DES_MAP.put("水瓶座","        水瓶座的人很聪明，他们最大的特点是创新，追求独一无二的生活，个人主义色彩很浓重的星座。他们对人友善又注重隐私。水瓶座绝对算得上是”友谊之星“，他喜欢结交每一类朋友，但是确很难与他们交心，那需要很长的时间。他们对自己的家人就显得冷淡和疏远很多了。");
        CONSTELLATION_DES_MAP.put("双鱼座","        双鱼座是十二宫最后一个星座，他集合了所有星座的优缺点于一身，同时受水象星座的情绪化影响，使他们原来复杂的性格又添加了更复杂的一笔。双鱼座的人最大的优点是有一颗善良的心，他们愿意帮助别人，甚至是牺牲自己。");

        CONSTELLATION_SEARCH_MAP.put(TYPE_VALUE_TODAY,"今日");
        CONSTELLATION_SEARCH_MAP.put(TYPE_VALUE_TOMORROW,"明日");
        CONSTELLATION_SEARCH_MAP.put(TYPE_VALUE_WEEK,"本周");
        CONSTELLATION_SEARCH_MAP.put(TYPE_VALUE_NEXTWEEK,"下周");
        CONSTELLATION_SEARCH_MAP.put(TYPE_VALUE_MONTH,"本月");
        CONSTELLATION_SEARCH_MAP.put(TYPE_VALUE_YEAR,"本年");

        PERSON_LIST.add("母亲");
        PERSON_LIST.add("父亲");
        PERSON_LIST.add("外公");
        PERSON_LIST.add("外婆");
        PERSON_LIST.add("儿子");
        PERSON_LIST.add("侄子");
        PERSON_LIST.add("哥哥");
        PERSON_LIST.add("弟弟");
        PERSON_LIST.add("姐姐");
        PERSON_LIST.add("妹妹");
        PERSON_LIST.add("舅妈");
        PERSON_LIST.add("表哥");
        PERSON_LIST.add("病人");
        PERSON_LIST.add("医生");
        PERSON_LIST.add("人群");
        PERSON_LIST.add("孩童");
        PERSON_LIST.add("年轻人");
        PERSON_LIST.add("帅哥");
        PERSON_LIST.add("嫂子");
        DREAM_TYPE_LIST_MAP.put("人物类",PERSON_LIST);
        ANIMAL_LIST.add("狗");
        ANIMAL_LIST.add("猫");
        ANIMAL_LIST.add("恐龙");
        ANIMAL_LIST.add("老虎");
        ANIMAL_LIST.add("蚂蚁");
        ANIMAL_LIST.add("鱼");
        ANIMAL_LIST.add("蛇");
        ANIMAL_LIST.add("龙");
        ANIMAL_LIST.add("鸟");
        ANIMAL_LIST.add("老鼠");
        ANIMAL_LIST.add("牛");
        ANIMAL_LIST.add("乌龟");
        ANIMAL_LIST.add("兔子");
        ANIMAL_LIST.add("狼");
        ANIMAL_LIST.add("猪");
        ANIMAL_LIST.add("熊");
        DREAM_TYPE_LIST_MAP.put("动物类",ANIMAL_LIST);

        QINGGAN_LIST.add("伤心");
        QINGGAN_LIST.add("哭泣");
        QINGGAN_LIST.add("大笑");
        QINGGAN_LIST.add("开心");
        QINGGAN_LIST.add("呼喊");
        QINGGAN_LIST.add("表白");
        QINGGAN_LIST.add("斥责");
        QINGGAN_LIST.add("生气");
        QINGGAN_LIST.add("分手");
        QINGGAN_LIST.add("哭");
        QINGGAN_LIST.add("离婚");
        QINGGAN_LIST.add("捉奸");
        QINGGAN_LIST.add("丑闻");
        DREAM_TYPE_LIST_MAP.put("情感类",QINGGAN_LIST);

        ZHIWU_LIST.add("丁香花");
        ZHIWU_LIST.add("丝瓜");
        ZHIWU_LIST.add("万年青");
        ZHIWU_LIST.add("中草药");
        ZHIWU_LIST.add("五谷");
        ZHIWU_LIST.add("树林");
        ZHIWU_LIST.add("仙人掌");
        ZHIWU_LIST.add("花");
        ZHIWU_LIST.add("大树");
        ZHIWU_LIST.add("山楂");
        ZHIWU_LIST.add("草");
        DREAM_TYPE_LIST_MAP.put("植物类",ZHIWU_LIST);

        LIFE_LIST.add("洗澡");
        LIFE_LIST.add("刷牙");
        LIFE_LIST.add("上厕所");
        LIFE_LIST.add("买菜");
        LIFE_LIST.add("上大学");
        LIFE_LIST.add("坐船");
        LIFE_LIST.add("坐电梯");
        LIFE_LIST.add("旅游");
        LIFE_LIST.add("买房子");
        LIFE_LIST.add("打篮球");
        DREAM_TYPE_LIST_MAP.put("生活类",LIFE_LIST);

        GUISHEN_LIST.add("上帝");
        GUISHEN_LIST.add("丘比特");
        GUISHEN_LIST.add("死人");
        GUISHEN_LIST.add("佛");
        GUISHEN_LIST.add("佛珠");
        GUISHEN_LIST.add("僵尸");
        GUISHEN_LIST.add("坟墓");
        GUISHEN_LIST.add("死人复活");
        GUISHEN_LIST.add("天使");
        DREAM_TYPE_LIST_MAP.put("鬼神类",GUISHEN_LIST);

        JIANZHU_LIST.add("书房");
        JIANZHU_LIST.add("商场");
        JIANZHU_LIST.add("仓库");
        JIANZHU_LIST.add("伊甸园");
        JIANZHU_LIST.add("会议室");
        JIANZHU_LIST.add("桥");
        JIANZHU_LIST.add("儿童乐园");
        JIANZHU_LIST.add("公园");
        JIANZHU_LIST.add("凉亭");
        JIANZHU_LIST.add("农场");
        JIANZHU_LIST.add("别墅");
        DREAM_TYPE_LIST_MAP.put("建筑类",JIANZHU_LIST);
    }

}
