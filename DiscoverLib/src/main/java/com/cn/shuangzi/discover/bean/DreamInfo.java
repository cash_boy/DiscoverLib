package com.cn.shuangzi.discover.bean;

import java.io.Serializable;

/**
 * Created by CN on 2017-12-20.
 */

public class DreamInfo implements Serializable{
    /**
     * id : 91e3f994eb2f601e5f8940a586e99015
     * des : 梦见死人或死亡不用害怕，不一定是坏事。
     * title : 死人
     */
    private String id;
    private String des;
    private String title;
    private String[] list;

    public String[] getList() {
        return list;
    }

    public void setList(String[] list) {
        this.list = list;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public String getDes() {
        return des;
    }

    public String getTitle() {
        return title;
    }
}
