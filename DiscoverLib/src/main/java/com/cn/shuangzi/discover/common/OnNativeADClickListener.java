package com.cn.shuangzi.discover.common;

import com.cn.shuangzi.discover.bean.NativeADModel;

/**
 * Created by CN.
 */

public interface OnNativeADClickListener {
    void onNativeADClick(NativeADModel nativeADModel,int position);
}
