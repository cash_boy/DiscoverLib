package com.cn.shuangzi.discover.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.cn.shuangzi.SZBaseActivity;
import com.cn.shuangzi.SZBaseFragment;
import com.cn.shuangzi.activity.SZInterfaceActivity;
import com.cn.shuangzi.adp.SZPagerAdp;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.common.DiscoverConst;
import com.cn.shuangzi.discover.fragment.ConstellationDayFragment;
import com.cn.shuangzi.discover.fragment.ConstellationMonthFragment;
import com.cn.shuangzi.discover.fragment.ConstellationWeekFragment;
import com.cn.shuangzi.discover.fragment.ConstellationYearFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by CN.
 */

public abstract class SZConstellationDetailActivity extends SZBaseActivity implements SZInterfaceActivity {
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView txtDesc;
    private String constellationName;
    private List<SZBaseFragment> fragmentList;

    @Override
    protected int onGetChildView() {
        return R.layout.activity_constellation_detail;
    }

    @Override
    protected void onBindChildViews() {
        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.viewPager);
        txtDesc = findViewById(R.id.txtDesc);
    }

    @Override
    protected void onBindChildListeners() {
    }

    @Override
    protected void onChildViewCreated() {
        constellationName = getStringExtra();
        if (constellationName == null) {
            finish();
            return;
        }
        txtDesc.setText(DiscoverConst.CONSTELLATION_DES_MAP.get(constellationName));
        setTitleTxt(constellationName);
        showBackImgLeft(getBackImgLeft());
        initViewPager();
        initTabLayout();
        fragmentList.get(0).onVisible();
    }

    @Override
    protected void onReloadData(boolean isRefresh) {

    }

    private void initTabLayout() {
        int colorNormal = getTabTextColorNormal() == 0 ? R.color.txtColorLightGray : getTabTextColorNormal();
        int colorSelect = getTabTextColorSelect() == 0 ? R.color.colorPrimary : getTabTextColorSelect();
        int colorTabBg = getTabBgColor() == 0 ? android.R.color.transparent : getTabBgColor();
        int colorIndicator = getSelectedTabIndicatorColor() == 0 ? R.color.colorPrimary : getSelectedTabIndicatorColor();
        tabLayout.setTabTextColors(getResources().getColor(colorNormal), getResources().getColor(colorSelect));
        tabLayout.setBackgroundColor(getResources().getColor(colorTabBg));
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(colorIndicator));
        tabLayout.setTabRippleColorResource(android.R.color.transparent);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initViewPager() {
        fragmentList = new ArrayList<>();
        ConstellationDayFragment constellationDayFragment = ConstellationDayFragment.newInstance(DiscoverConst.TYPE_VALUE_TODAY, constellationName);
        constellationDayFragment.setTitleShow(DiscoverConst.CONSTELLATION_SEARCH_MAP.get(DiscoverConst.TYPE_VALUE_TODAY));
        fragmentList.add(constellationDayFragment);
        constellationDayFragment = ConstellationDayFragment.newInstance(DiscoverConst.TYPE_VALUE_TOMORROW, constellationName);
        constellationDayFragment.setTitleShow(DiscoverConst.CONSTELLATION_SEARCH_MAP.get(DiscoverConst.TYPE_VALUE_TOMORROW));
        fragmentList.add(constellationDayFragment);
        ConstellationWeekFragment constellationWeekFragment = ConstellationWeekFragment.newInstance(DiscoverConst.TYPE_VALUE_WEEK, constellationName);
        constellationWeekFragment.setTitleShow(DiscoverConst.CONSTELLATION_SEARCH_MAP.get(DiscoverConst.TYPE_VALUE_WEEK));
        fragmentList.add(constellationWeekFragment);
        constellationWeekFragment = ConstellationWeekFragment.newInstance(DiscoverConst.TYPE_VALUE_NEXTWEEK, constellationName);
        constellationWeekFragment.setTitleShow(DiscoverConst.CONSTELLATION_SEARCH_MAP.get(DiscoverConst.TYPE_VALUE_NEXTWEEK));
        fragmentList.add(constellationWeekFragment);
        ConstellationMonthFragment constellationMonthFragment = ConstellationMonthFragment.newInstance(DiscoverConst.TYPE_VALUE_MONTH,constellationName);
        constellationMonthFragment.setTitleShow(DiscoverConst.CONSTELLATION_SEARCH_MAP.get(DiscoverConst.TYPE_VALUE_MONTH));
        fragmentList.add(constellationMonthFragment);
        ConstellationYearFragment constellationYearFragment = ConstellationYearFragment.newInstance(DiscoverConst.TYPE_VALUE_YEAR,constellationName);
        constellationYearFragment.setTitleShow(DiscoverConst.CONSTELLATION_SEARCH_MAP.get(DiscoverConst.TYPE_VALUE_YEAR));
        fragmentList.add(constellationYearFragment);
        viewPager.setOffscreenPageLimit(6);
        viewPager.setAdapter(new SZPagerAdp(getSupportFragmentManager(), fragmentList));
    }

    public int getTabTextColorNormal() {
        return 0;
    }

    public int getTabTextColorSelect() {
        return 0;
    }

    public int getSelectedTabIndicatorColor() {
        return 0;
    }


    @Override
    protected long getDelayTimeOnLoadChildView() {
        return 100;
    }

    public int getTabBgColor() {
        return 0;
    }
}
