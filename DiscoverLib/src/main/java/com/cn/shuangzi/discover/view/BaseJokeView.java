package com.cn.shuangzi.discover.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.aspsine.irecyclerview.IRecyclerView;
import com.aspsine.irecyclerview.OnLoadMoreListener;
import com.aspsine.irecyclerview.OnRefreshListener;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.cn.shuangzi.discover.DiscoverManager;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.RandomBean;
import com.cn.shuangzi.discover.common.HttpRequest;
import com.cn.shuangzi.discover.common.NativeADConst;
import com.cn.shuangzi.util.SZIRecyclerViewUtil;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Created by CN.
 */

public abstract class BaseJokeView extends RelativeLayout implements OnRefreshListener, OnLoadMoreListener {
    protected IRecyclerView recyclerView;
    protected Activity activity;

    private int colorSpaceItem = Color.parseColor("#EEEEEE");
    private List<MultiItemEntity> dataList;
    private OnLoadListener onLoadListener;
//    public BaseJokeView(Context context) {
//        super(context);
//        initView();
//    }

    public BaseJokeView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(attrs);
    }

    public BaseJokeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(attrs);
    }

    public void loadData(Activity activity) {
        this.activity = activity;
        onRefresh();
    }

    private void initView(AttributeSet attrs) {
        if (DiscoverManager.application == null) {
            throw new IllegalArgumentException("joke do not init...");
        }
        initParams(attrs);
        dataList = new ArrayList<>();
        inflate(getContext(), R.layout.layout_joke, this);
        recyclerView = findViewById(R.id.recyclerView);
        SZIRecyclerViewUtil.setVerticalLinearLayoutManager(getContext(), recyclerView,colorSpaceItem,1);
        recyclerView.setOnRefreshListener(this);
        recyclerView.setOnLoadMoreListener(this);
    }

    private void initParams(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.JokeView);
        int n = typedArray.getIndexCount();
        for (int i = 0; i < n; i++) {
            int attr = typedArray.getIndex(i);
            if(attr == R.styleable.JokeView_colorSpaceItem){
                colorSpaceItem = typedArray.getColor(R.styleable.JokeView_colorSpaceItem, Color.parseColor("#EEEEEE"));
            }
        }
        typedArray.recycle();
    }

    @Override
    public void onRefresh() {
        loadData(true);
    }

    @Override
    public void onLoadMore() {
        loadData(false);
    }


    public void loadData(final boolean isRefresh) {
        HttpRequest.requestNoNeedAnalyze(NativeADConst.BASE_URL_JOKE_RANDOM, getParams(), getRequestTag(), new HttpRequest.HttpResponseSimpleListener() {

            @Override
            public void onNetError(String url, int errorCode) {
                Toast.makeText(getContext(), getContext().getString(R.string.error_net_work), Toast.LENGTH_SHORT).show();
                if (onLoadListener != null) {
                    onLoadListener.onError(!isRefresh);
                }
                SZIRecyclerViewUtil.judgePullRefreshStatus(recyclerView, Integer.MAX_VALUE);
            }

            @Override
            public void onSuccess(String url, String responseData) {
                if(isRefresh){
                    if (dataList == null) {
                        dataList = new ArrayList<>();
                    } else {
                        dataList.clear();
                    }
        				setAdapterData(dataList);
                }
                RandomBean mRandomBean = new Gson().fromJson(responseData, RandomBean.class);
                if (mRandomBean != null && mRandomBean.result != null && mRandomBean.result.size() > 0) {
                    List<RandomBean.ResultBean> resultBeanList = mRandomBean.result;
                    dataList.addAll(resultBeanList);
                }
                if (onLoadListener != null) {
                    onLoadListener.onSuccess();
                }
                setAdapterData(dataList);
                SZIRecyclerViewUtil.judgePullRefreshStatus(recyclerView, Integer.MAX_VALUE);
            }
        });
    }

    private void loadAD() {
    }

    private void loadTTAD() {
    }

    private void loadGDTAD() {
    }
    public abstract int getADImgWidth();
    public abstract int getADImgHeight();
    public abstract Map<String, String> getParams();

    public abstract String getRequestTag();

    public abstract void setAdapterData(List<MultiItemEntity> multiItemEntityList);

    public OnLoadListener getOnLoadListener() {
        return onLoadListener;
    }

    public void setOnLoadListener(OnLoadListener onLoadListener) {
        this.onLoadListener = onLoadListener;
    }

    public interface OnLoadListener {
        void onSuccess();

        void onError(boolean isLoadMore);
    }
}
