package com.cn.shuangzi.discover.adp;

import android.app.Activity;

import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.cn.shuangzi.discover.R;
import com.cn.shuangzi.discover.bean.RandomBean;
import com.cn.shuangzi.discover.common.NativeADConst;
import com.cn.shuangzi.discover.common.OnNativeADClickListener;

import java.util.List;


public class RandomTextJokeAdp extends BaseJokeAdp {

    public RandomTextJokeAdp(final Activity activity, final List<MultiItemEntity> multiItemEntityList, OnNativeADClickListener onNativeADClickListener) {
        super(activity, multiItemEntityList, null,onNativeADClickListener,false);
    }


    @Override
    public void addItemTypes() {
        addItemType(NativeADConst.TYPE_DATA, R.layout.adp_txt_joke);
    }

    @Override
    protected void convert(BaseViewHolder helper, MultiItemEntity item) {
        switch (helper.getItemViewType()) {
            case NativeADConst.TYPE_DATA:
                loadDataView(helper, item);
                break;
            case NativeADConst.TYPE_GDT:
                loadGDTView(helper,item);
                break;
            case NativeADConst.TYPE_TT:
                loadTTCommonView(helper,item);
                break;
        }
    }
    private void loadDataView(BaseViewHolder helper, MultiItemEntity item) {
        helper.setText(R.id.tv_content,((RandomBean.ResultBean)item).content);
    }
    @Override
    public void loadTTView(BaseViewHolder helper, MultiItemEntity item) {
    }
}
